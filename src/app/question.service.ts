import { Injectable } from '@angular/core';
import { Question } from './models/question.model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  questions: Question[];
  constructor() {
    this.questions = [
      {
        id: 1,
        body: 'Bonjour jeune chevalier ! Êtes-vous prêt à vivre une grande aventure ?',
        answers: [{
          id: 1,
          body: 'Oui ! Allons-y !',
          nextQuestion: 11
        }, {
          id: 2,
          body: 'Je sais pas trop, j\'hésite...',
          nextQuestion: 12
        }, {
          id: 3,
          body: 'Non trop la flemme là...',
          nextQuestion: 13
        }]
      }, {
        id: 11,
        body: 'C\'est parti alors ! Montez sur votre fidèle destrier, et vous êtes parti ! Votre premier choix arrive quelle destination allez-vous prendre ?',
        answers: [{
          id: 1,
          body: 'Direction Gul\'Tak',
          nextQuestion: 111
        }, {
          id: 2,
          body: 'Direction Blancherive',
          nextQuestion: 112
        }, {
          id: 3,
          body: 'Vous préférez discuter avec le marchand à 10 mètres de vous',
          nextQuestion: 113
        }]
      }, {
        id: 113,
        body: 'Oy, jeune voyageur ! Voulez-vous acheter une de mes pommes, une achetée, une offerte !',
        answers: [{
          id: 1,
          body: 'Non merci, savez-vous où je pourrai vivre une grande aventure ?',
          nextQuestion: 1131
        }, {
          id: 2,
          body: 'Avec plaisir ! Je prendrai cette pomme bleue !',
          nextQuestion: 1132
        }, {
          id: 3,
          body: 'Mhhhh...Pourquoi pas !',
          nextQuestion: 1133
        }]
      },
      {
        id: 1131,
        body: 'Mhhhh, je vous recommande d\'aller à Incarnam, c\'est une petite citée mais les plus grands chevalier de notre monde viennent tous de là bas... Je me demande bien pourquoi !',
        answers: [{
          id: 1,
          body: 'Partir pour Incarnam',
          nextQuestion: 11311
        }, {
          id: 2,
          body: 'Être dubitatif.',
          nextQuestion: 11312
        }]
      }
    ]
  }

  findAll(): Question[] {
    return this.questions;
  }

  findOne(id: number): Question {
    let question = null;
    this.questions.forEach(q => {
      if (id === q.id) {
        question = q;
      }
    });

    return question;
  }

  save(q: Question): void {
    this.questions.push(q);
  }
}
