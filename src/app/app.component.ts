import { Component } from '@angular/core';
import { Question } from './models/question.model';
import { QuestionService } from './question.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Livre Jeu';

  question: Question;

  constructor(private questionService: QuestionService) {
    this.question = this.questionService.findOne(1);
  }


}
