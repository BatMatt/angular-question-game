import { Question } from './question.model';

export class Answer {
    id: number;
    body: string;
    nextQuestion: number;
}