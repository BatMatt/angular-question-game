import { Answer } from './answer.model';


export class Question {
    id: number;
    body: string;
    answers: Answer[];
}