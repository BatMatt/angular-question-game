import { Component, OnInit, Input, Output } from '@angular/core';
import { Question } from '../models/question.model';
import { EventEmitter } from 'events';
import { QuestionService } from '../question.service';
import { Answer } from '../models/answer.model';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input() question: Question;

  constructor(private questionService: QuestionService) { }

  ngOnInit() {
  }

  loadNextQuestion(answer: Answer) {
    const question = this.questionService.findOne(answer.nextQuestion);

    this.question = question;
  }

}
